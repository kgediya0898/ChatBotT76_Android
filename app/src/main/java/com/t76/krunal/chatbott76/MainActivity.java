﻿package com.t76.krunal.chatbott76;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.MapFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Array;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    TextToSpeech t1;
    LocationManager lcm;
    public String response;
    BufferedReader reader;
    HttpURLConnection connection;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_main);

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });
    }
   public void onSend(View v){


       EditText editv =(EditText)findViewById(R.id.sendBox);
       final ScrollView scrv =(ScrollView)findViewById(R.id.scrollview1) ;
       TextView textView1 = new TextView(this);
       RelativeLayout rlv =(RelativeLayout)findViewById(R.id.relative);
       String msg = editv.getText().toString();
       GradientDrawable bgshape3 =(GradientDrawable)rlv.getBackground();
       LinearLayout scv = (LinearLayout) findViewById(R.id.linear_yout_scroll) ;
       LinearLayout linearlayout =(LinearLayout)findViewById(R.id.linear_layout);
       textView1.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
               ActionBar.LayoutParams.MATCH_PARENT));

       textView1.setText("You : "+msg);

       textView1.setMaxLines(1);
       textView1.setMinWidth(250);
       textView1.setMinHeight(50);
       textView1.setTextSize(20);

       textView1.setBackgroundResource(R.drawable.shape); // hex color 0xAARRGGBB
       GradientDrawable bgShape1 = (GradientDrawable)textView1.getBackground();
       textView1.setTextColor(getResources().getColor(R.color.light));

       scv.addView(textView1);

TextView sendButton =(TextView)findViewById(R.id.button_send);
       textView1.setId(textView1.generateViewId());
       TextView vartxt =(TextView)findViewById(textView1.getId());
       ViewGroup.MarginLayoutParams vartxt1=(ViewGroup.MarginLayoutParams)vartxt.getLayoutParams();
       vartxt1.setMargins(25,25,0,0);
       vartxt.setPadding(15,15,15,15);
       vartxt.setGravity(Gravity.CENTER);
       String response = editv.getText().toString().toLowerCase();
       TextView textView2 = new TextView(this);
       textView2.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
               ActionBar.LayoutParams.MATCH_PARENT));

       textView2.setBackgroundResource(R.drawable.shapesecond); // hex color 0xAARRGGBB
       GradientDrawable bgShape2 = (GradientDrawable)textView2.getBackground();
       String responset76="Sorry I didn't catch that \n"+"Try saying help to see available things you can try";
       String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
       String weburl;
       textView2.setTextColor(getResources().getColor(R.color.light));
       scv.addView(textView2);

       textView2.getParent().requestChildFocus(textView2,textView2);
       String jokes[]={"Why did the physics teacher break up with the biology teacher? There was no chemistry","Can a kangaroo jump higher than a house? Of course, a house doesn’t jump at all.","Some nice Chinese couple gave me a very good camera down by the Washington Monument. I didn’t really understand what they were saying, but it was very nice of them."};//Add the jokes here
       int joke=jokes.length;
       String wishes[]={"Jai Hind", "Meraa Bhaarat Maahaan",response,"Same to you my friend"};
       int wish=wishes.length;
       String greetings[]={"Hi","Hello","Namaste","Hi there","Hello User","Namaste \n What can I do for you?"};//Add greetings here
       int greeting=greetings.length;
       String errors[]={"Sorry I didn't understand \n Type Quick to quickview websites","Sorry I didn't catch that \n Type Play to play games on the go","Can you rephrase that? \n Want directions? Try typing directions from startPlace to endPlace","I don't talk Gibberish","Unfortunately I can't parse that information for now \n Try typing set theme to change the current theme","Can you repeat that again?? \n Try typing set language to change language"};//Add error messages
       int error=errors.length;
       Random l= new Random();
       int o=l.nextInt(error);
       responset76=errors[o];
       String start;
       String end;
       start="Boomerang";
       end="Andheri";
GradientDrawable bgshape4 =(GradientDrawable)linearlayout.getBackground();
       if(response.contains("How are You".toLowerCase())){
responset76="I'm fine";
   ;}
       else if (response.contains("name".toLowerCase())&&response.contains("your".toLowerCase())){
    responset76="My name is T76 chatbot";
}
       else if (response.contains("location".toLowerCase())&&response.contains("".toLowerCase())){

           //  responset76="My name is T76 chatbot";
       }
else if(response.contains("who")&&response.contains("created")&&response.contains("you")){
    responset76="I'm created by Krunal";
}
else if (response.contains("Happy".toLowerCase())&&response.contains(" day".toLowerCase())){
    responset76="Same to you my friend";
}

else if (response.contains("Happy".toLowerCase())&&response.contains("birthday".toLowerCase())){
    responset76="Thank you. But i have birthdays everytime i get updated. So in that sense, you will have to wish me every now and then";
}

       else if(response.equalsIgnoreCase("Hi")||response.contains("Hi ".toLowerCase())||response.contains("Hello".toLowerCase())||response.contains("Namaste".toLowerCase())){
    Random n = new Random();
    int i=n.nextInt(greeting);
           responset76=greetings[i];



       }
else if(response.contains("tell ".toLowerCase())&&response.contains("joke".toLowerCase())){
    Random n = new Random();
    int i=n.nextInt(joke);
           responset76=jokes[i];




}

       else if(response.contains("who")&&response.contains("are")&&response.contains("you")){
    responset76="I'm T76 , Your chatbot assistant";
       }

       else if(response.contains("Who".toLowerCase())&& response.contains("siri".toLowerCase())){
           responset76="That's a Sereeeeeeeeee us question";
       }
       else if(response.contains("Who".toLowerCase())&& response.contains("google".toLowerCase())){
           responset76="That's a googlie question";
       }
       else if(response.equalsIgnoreCase("themes")){
           responset76="Violet, Blue, Black, Red, Default";
       }
       else if(response.contains("set language".toLowerCase())) {
           if (response.contains("Korean".toLowerCase())) {
               t1.setLanguage(Locale.KOREAN);
               responset76="Language Set To Korean";

           } else if (response.contains("Canada".toLowerCase())) {
               t1.setLanguage(Locale.CANADA);
               responset76="Language Set To Canadian";

           }
           else if (response.contains("China".toLowerCase())) {
               t1.setLanguage(Locale.CHINA);
               responset76="Language Set To Chinese";

           }
           else if (response.contains("UK".toLowerCase())) {
               t1.setLanguage(Locale.UK);
               responset76="Language Set To United Kingdom";

           }
           else if (response.contains("US".toLowerCase())) {
               t1.setLanguage(Locale.US);
               responset76="Language Set To US";

           } else {
               responset76 = "Sorry\n" + "Currently you can only set languages to US,UK,Canada And Korean";
               //weburl = "https://www.google.co.in/webhp?sourceid=chrome-instant&rlz=1C1CHBF_enIN728IN728&ion=1&espv=2&ie=UTF-8#safe=off&q=" + response;
           }
       }
       else if(response.contains("set theme".toLowerCase())) {
           if (response.contains("blue".toLowerCase())) {
               editv.setTextColor(0xff0277BD);
               sendButton.setTextColor(0xff0277BD);
               bgShape1.setColor(0xff0277BD);
               bgShape2.setColor(0xff0277BD);
               bgshape3.setColor(0xffffffff);
               bgshape4.setColor(0xff0277BD);
               responset76="Theme set to Blue";
           } else if (response.contains("red".toLowerCase())) {
               editv.setTextColor(0xffb71c1c);
               sendButton.setTextColor(0xffb71c1c);
               bgShape1.setColor(0xffb71c1c);
               bgShape2.setColor(0xffb71c1c);
               bgshape3.setColor(0xffffffff);
               bgshape4.setColor(0xffb71c1c);
               responset76="Theme set to Red";
           }
           else if (response.contains("violet".toLowerCase())) {
               editv.setTextColor(0xff6A1B9A);
               sendButton.setTextColor(0xff6A1B9A);
               bgShape1.setColor(0xff6A1B9A);
               bgShape2.setColor(0xff6A1B9A);
               bgshape3.setColor(0xffffffff);
               bgshape4.setColor(0xff6A1B9A);
               responset76="Theme set to Violet";
           }
           else if (response.contains("black".toLowerCase())) {
               editv.setTextColor(0xff000000);
               sendButton.setTextColor(0xff000000);
               bgShape1.setColor(0xff000000);
               bgShape2.setColor(0xff000000);
               bgshape3.setColor(0xffffffff);
               bgshape4.setColor(0xff000000);
               responset76="Theme set to Black";
               //getTheme().applyStyle(R.style.Dark, true);
               //rlv.setBackgroundResource();


           }
           else if (response.contains("default".toLowerCase())) {
               editv.setTextColor(0xff37474f);
               sendButton.setTextColor(0xff37474f);
               bgShape1.setColor(0xff607D8B);
               bgShape2.setColor(0xff212121);
               bgshape3.setColor(0xffffffff);
               bgshape4.setColor(0xff37474f);
               responset76="Theme reverted to default";
               //getTheme().applyStyle(R.style.Dark, true);
               //rlv.setBackgroundResource();


           }else {
               responset76 = "Sorry\n" + "No more themes available for now.Type themes to check the available themes";
               //weburl = "https://www.google.co.in/webhp?sourceid=chrome-instant&rlz=1C1CHBF_enIN728IN728&ion=1&espv=2&ie=UTF-8#safe=off&q=" + response;
           }
       }

       else if(response.contains("quick".toLowerCase()))
{
    if(response.contains("facebook".toLowerCase())){
        responset76="Facebook?? Here you go";
        weburl="https://facebook.com/";
    }
    else if(response.contains("google".toLowerCase()))
    {
        responset76="Wanna search something? Google is Here";
        weburl="https://www.google.com/";
    }

    else{
       response=response.replace("quick","");
        responset76="Sorry\n"+"Currently you can only quickview google and facebook \n"+"I can search the web for it though";
        weburl="https://www.google.co.in/search?sclient=psy-ab&safe=off&rlz=1C1CHBF_enIN728IN728&biw=1532&bih=716&noj=1&site=webhp&q="+response+"&oq="+response+"&gs_l=serp.3..0l4j0i131k1j0l5.12408.13860.1.15086.5.5.0.0.0.0.545.973.4-1j1.2.0....0...1c.1.64.serp..3.2.973...35i39k1.uTlw4_is8_Y";
    }

    WebView webv =new WebView(this);
    webv.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.WRAP_CONTENT));
    WebSettings settings = webv.getSettings();
    settings.setJavaScriptEnabled(true);

    webv.loadUrl(weburl);


    webv.setWebViewClient(new WebViewClient());

    scv.addView(webv, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1000));
    webv.setId(webv.generateViewId());
    WebView webv1 = (WebView) findViewById(webv.getId());
    ViewGroup.MarginLayoutParams webvm=(ViewGroup.MarginLayoutParams)webv1.getLayoutParams();
    webvm.setMargins(25,25,25,0);
    webv1.getParent().requestChildFocus(webv1,webv1);


} else if(response.contains("directions".toLowerCase())||response.contains("locate".toLowerCase()))

       {
           if (response.contains("where".toLowerCase()) && response.contains("is".toLowerCase())) {
               if (response.contains("at".toLowerCase())) {
                   String testStr = response;
                   String result[] = testStr.split("\\s+");
                   int n = Arrays.asList(result).indexOf("at");
                   start = result[n + 1];
               }
               String testStr = response;
               String result[] = testStr.split("\\s+");
               int n = Arrays.asList(result).indexOf("is");
               end = result[n + 1];
           } else if (response.contains("from".toLowerCase())) {

               String testStr = response;

               String result[] = testStr.split("\\s+");
               int n = Arrays.asList(result).indexOf("from");
               start = result[n + 1];
               int m = Arrays.asList(result).indexOf("to");
               end = result[m + 1];

           } else {
               responset76 = "Here are your directions from " + start + " to " + end;
               weburl = "https://www.google.co.in/maps/dir/" + start + "/" + end + "/@19.0616258,72.8240724,16z";

               WebView webv = new WebView(this);
               webv.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                       ActionBar.LayoutParams.WRAP_CONTENT));
               WebSettings settings = webv.getSettings();
               settings.setJavaScriptEnabled(true);

               webv.loadUrl(weburl);


               webv.setWebViewClient(new WebViewClient());

               scv.addView(webv, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1000));
               webv.setId(webv.generateViewId());
               WebView webv1 = (WebView) findViewById(webv.getId());
               ViewGroup.MarginLayoutParams webvm = (ViewGroup.MarginLayoutParams) webv1.getLayoutParams();
               webvm.setMargins(25, 25, 25, 0);
               webv1.getParent().requestChildFocus(webv1, webv1);


           }
       }
       else if(response.contains("neebal".toLowerCase())){
           responset76="Neebal is a Software company in Mumbai, India";
       }
       else if(response.contains("tell".toLowerCase())||(response.contains("know".toLowerCase()))&&response.contains("about".toLowerCase()))

       {
                   String testStr= response;
                   String result[]=testStr.split("\\s+");
                   int n= Arrays.asList(result).indexOf("about");
                   start=result[n+1];

           responset76="Here are the matching results";
           weburl="https://www.google.co.in/search?sclient=psy-ab&safe=off&rlz=1C1CHBF_enIN728IN728&biw=1532&bih=716&noj=1&site=webhp&q="+start+"&oq="+start+"&gs_l=serp.3..0l4j0i131k1j0l5.12408.13860.1.15086.5.5.0.0.0.0.545.973.4-1j1.2.0....0...1c.1.64.serp..3.2.973...35i39k1.uTlw4_is8_Y";

           WebView webv =new WebView(this);
           webv.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
                   ActionBar.LayoutParams.WRAP_CONTENT));
           WebSettings settings = webv.getSettings();
           settings.setJavaScriptEnabled(true);

           webv.loadUrl(weburl);


           webv.setWebViewClient(new WebViewClient());

           scv.addView(webv, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 500));
           webv.setId(webv.generateViewId());
           WebView webv1 = (WebView) findViewById(webv.getId());
           ViewGroup.MarginLayoutParams webvm=(ViewGroup.MarginLayoutParams)webv1.getLayoutParams();
           webvm.setMargins(25,25,25,0);
           webv1.getParent().requestChildFocus(webv1,webv1);


       }
else if(response.contains("play".toLowerCase()))
{
    if(response.contains("tic tac toe".toLowerCase())||response.contains("x and o")){
        responset76="I hope you win";
        weburl="https://playtictactoe.org/";
    }
    else if(response.contains("Chess".toLowerCase()))
    {
        responset76="I want to see how smart you are";
        weburl="https://www.sparkchess.com/";
    }
    else{
        responset76="Sorry\n"+"I have only tic tac toe and chess to play with for now\n"+ "Until we add more you can play this one";
        weburl="http://apps.thecodepost.org/trex/trex.html";
    }

    WebView webv =new WebView(this);
    webv.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.WRAP_CONTENT));
    WebSettings settings = webv.getSettings();
    settings.setJavaScriptEnabled(true);

    webv.loadUrl(weburl);


    webv.setWebViewClient(new WebViewClient());

    scv.addView(webv, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    webv.setId(webv.generateViewId());
    WebView webv1 = (WebView) findViewById(webv.getId());
    ViewGroup.MarginLayoutParams webvm=(ViewGroup.MarginLayoutParams)webv1.getLayoutParams();
    webvm.setMargins(25,25,25,0);
    webv1.getParent().requestChildFocus(webv1,webv1);


}
else if(response.equalsIgnoreCase("help")||response.contains("what can you do".toLowerCase())){
           responset76="You can try saying play, or quick to play games and surf the internet on the go";
       }

       else if(response.contains("can you".toLowerCase())){
           if(response.contains("time")){
               responset76="Yes ofcourse I can. Try saying time";
           }
           else if(response.contains("play")){
               responset76="Sure I can. Try saying play";
           }
           else {
               responset76="Not yet. But my developers are working hard everyday to improve me";
           }
       }
else if(response.contains("Thanks".toLowerCase())||response.contains("Thank you")||response.contains("shukriya")){
    responset76="You are welcome. Im glad to help you";
}
else if(response.contains("I".toLowerCase())&&response.contains("bored".toLowerCase())){
    responset76="Wanna play some games?? Try saying play tic tac toe or play chess";
}
else if(response.contains("time".toLowerCase())) {
   if (response.contains("USA".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT-05:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("UK".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT00:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Argentina".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT-03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Afghanistan".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+04:30");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Bangladesh".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+06:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Austria".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Belgium".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Bhutan".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+06:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Brazil".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT−04:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Cambodia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+07:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Cameroon".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Chile".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT−04:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("China".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+08:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Colombia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT−05:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Cuba".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT-05:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Czech Republic".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Denmark".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Dominican Republic".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT−04:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Egypt".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Ethiopia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+07:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Fiji".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+12:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Finland".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("France".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Georgia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+04:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Germany".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Ghana".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT±00:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Greece".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Greenland ".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT−03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Indonesia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+08:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Iran".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:30");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Iraq".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Ireland".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT±00:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Israel".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Italy".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Japan".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+09:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Jamaica".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT−05:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Jersey ".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT±00:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Jordan".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Kenya".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Liberia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT±00:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Libya".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Madagascar".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Malaysia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+08:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Maldives".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+05:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Mexico".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT−06:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Monaco".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Morocco".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT±00:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Nepal".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+05:45");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Netherlands".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Namibia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("New Caledonia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+11:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("New Zealand".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+12:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Nigeria".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("North Korea".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+08:30");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Norway".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Oman".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+04:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Pakistan".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+05:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Peru".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT−05:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Philippines".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+08:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Poland".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Portugal".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT±00:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Qatar".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Romania".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Russia ".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Saudi Arabia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Serbia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Singapore".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+08:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Slovakia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Slovenia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Solomon Islands".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+11:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("South Africa".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("South Korea".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+09:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Spain".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Sri Lanka".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+05:30");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Sudan".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Sweden".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Switzerland".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+01:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Syria".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Taiwan".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+08:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Tanzania".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Thailand".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+07:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Turkey".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Uganda".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Ukraine".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("United Arab Emirates".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+04:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Uruguay".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT-03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Uzbekistan".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+05:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Vietnam".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+07:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Yemen".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+03:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    } else if (response.contains("Zambia".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    }
   else if (response.contains("Australia".toLowerCase())) {

       java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+10:00");
       java.util.Calendar c = java.util.Calendar.getInstance(tz);

       responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
   }
   else if (response.contains("Zimbabwe".toLowerCase())) {

        java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+02:00");
        java.util.Calendar c = java.util.Calendar.getInstance(tz);

        responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
    }
    else {
       java.util.TimeZone tz = java.util.TimeZone.getTimeZone("GMT+05:30");
       java.util.Calendar c = java.util.Calendar.getInstance(tz);

       responset76 = "The time is "+c.get(java.util.Calendar.HOUR_OF_DAY) + ":" + c.get(java.util.Calendar.MINUTE) + ":" + c.get(java.util.Calendar.SECOND);
   }
}
//source https://www.countries-ofthe-world.com/world-time-zones.html


       textView2.setText("T76 : "+responset76);


    textView2.setMinWidth(250);
    textView2.setMinHeight(50);
    textView2.setTextSize(20);
       t1.speak(responset76, TextToSpeech.QUEUE_FLUSH, null);


    textView2.setId(textView1.generateViewId());
    TextView vartxt3 =(TextView)findViewById(textView2.getId());
    ViewGroup.MarginLayoutParams vartxt2=(ViewGroup.MarginLayoutParams)vartxt3.getLayoutParams();
    vartxt2.setMargins(25,25,25,25);
    vartxt3.setPadding(15,15,15,15);
    vartxt3.setGravity(Gravity.CENTER);
       editv.setText("");

   }
    public void ifElse(){

    }
    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }}
